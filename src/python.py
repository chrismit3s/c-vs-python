from timeit import timeit


def gcd(a, b):
    return (_gcd(b, a)
            if (a < b)
            else _gcd(a, b))


def _gcd(a, b):
    if b == 0:
        return a
    return _gcd(b, a % b)


def fibonacci(n):
    a = 0
    b = 1
    for i in range(n):
        c = a + b
        a = b
        b = c
    return a


def average(data):
    avg = 0.0
    for i, x in enumerate(data):
        avg += (x - avg) / (i + 1)
    return avg


arr = [-609894693, -644820087,  124052517,  578449504,  177189535,
       -541201729,  251933844, -781150168, -618969099,  -95385066,
        886154531,  700685743,  224770582,   32996367, -660863892,
        207931729, -795317126, -958050375, -606250446, -420795577,
        930179660, -984092078, -466757764, -567485493, -250321746,
        533600718, -844505521,  735141238,  968879328, -785249928,
       -128749453,  159399310, -198934019, -666963979,  -50742344,
         67537592,  165760819,  760253692,  109710605,    3763981,
       -962509976,  479027692,  688679265,  833500680, -830116484,
        549490924, -301367194, -891717846, -127859659,  539947165,
       -227268104, -892776216,  454182480, -168434084, -473160314,
       -799225585,  -81941356, -382886021,  618623075, -151646610,
        610404373,  106900286, -352546003,   33295777,  533786504,
        -32749817, -171702882,  774843120,  164588322, -686770749,
        834466041, -953453133,  103186273,  191541852,  707456096,
       -783845394,  693103278, -631821908,  436007327, -636139355,
        578691297,  184049779, -699683650,  403613392,  706911713,
        490821463,  535389722,   81562709,  554617353, -806513389,
       -632716699, -516570878,  673667414, -129860055,  166527114,
        390291519,  131190489, -278349424, -707297473, -358951089]
num = 1000000


print(f"gcd:       {timeit(stmt='gcd(1089, 624)', number=num, globals=globals()) * 1000:.0f} ms")
print(f"fibonacci: {timeit(stmt='fibonacci(32)', number=num, globals=globals()) * 1000:.0f} ms")
print(f"average:   {timeit(stmt='average(arr)', number=num, globals=globals()) * 1000:.0f} ms")
