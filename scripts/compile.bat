@echo off

gcc src\c.c -O0 -o bin\c-o0.exe
gcc src\c.c -O1 -o bin\c-o1.exe
gcc src\c.c -O2 -o bin\c-o2.exe
gcc src\c.c -O3 -o bin\c-o3.exe

REM .\venv\Scripts\activate.bat
py -3.7 -m PyInstaller src\python.py -F --distpath bin --workpath temp > nul 2>&1
del python.spec