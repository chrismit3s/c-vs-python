@echo off

REM .\venv\Scripts\activate.bat

echo C o0
bin\c-o0.exe
echo.

echo C o1
bin\c-o1.exe
echo.

echo C o2
bin\c-o2.exe
echo.

echo C o3
bin\c-o3.exe
echo.

echo Python interpreted
py -3 src\python.py
echo.

echo Python compiled
bin\python.exe